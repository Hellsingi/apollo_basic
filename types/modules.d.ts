declare namespace NodeJS {
    export interface ProcessEnv {
        DB_USERNAME: string;
        DB_DATABASE: string;
        DB_PASSWORD: string;
    }
}