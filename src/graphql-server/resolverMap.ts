const bcrypt = require('bcryptjs')

const resolverMap = {
    Query: {
        async user(root: string, { id }: { id: number }, { models }: { models: any }) {
            return models.User.findById(id)
        },
        async allRecipes(root: string, args: any[], { models }: { models: any }) {
            return models.Recipe.findAll()
        },
        async recipe(root: string, { id }: { id: number }, { models }: { models: any }) {
            return models.Recipe.findById(id)
        }
    },

    Mutation: {
        async createUser(root: string, { name, email, password }: { name: string, email: string, password: string }, { models }: { models: any }) {
            return models.User.create({
                name,
                email,
                password: await bcrypt.hash(password, 10)
            })
        },
        async createRecipe(root: string, { userId, title, ingredients, direction }: { userId: number, title: string, ingredients: string, direction: string }, { models }: { models: any }) {
            return models.Recipe.create({ userId, title, ingredients, direction })
        }
    },

    User: {
        async recipes(user: any) {
            return user.getRecipes()
        }
    },
    Recipe: {
        async user(recipe: any) {
            return recipe.getUser()
        }
    }
}

module.exports = resolverMap