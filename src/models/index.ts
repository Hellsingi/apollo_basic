'use strict';

// const fs = require('fs');
// const path = require('path');
// const Sequelize = require('sequelize');
// const basename = path.basename(__filename);
// const env = process.env.NODE_ENV || 'development';
// const config = require(__dirname + '/../config/config.json')[env];
// const db = {};

import { Sequelize, DataTypes } from 'sequelize';
import * as fs from 'fs';
import * as path from 'path';

import { User } from './user';
import { Recipe } from './recipe';
require('dotenv').config();

const config = require(__dirname + '../config/config.json');

// const dbConfig = config[process.env.NODE_ENV];

const sequelize = new Sequelize(
    // dbConfig['database'],
    // dbConfig['username'],
    // dbConfig['password'],
    config[process.env.DB_USERNAME],
    config[process.env.DB_PASSWORD],
    config[process.env.DB_DATABASE],
    config
);

User.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
  tableName: 'Users',
  sequelize
});


Recipe.init(
  {
    titile: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ingredients: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    direction: {
      type: DataTypes.TEXT,
      allowNull: false
    },
  }, {
  tableName: 'Resipes',
  sequelize: sequelize, // this bit is important
});

export default sequelize;


// fs
//   .readdirSync(__dirname)
//   .filter(file => {
//     return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
//   })
//   .forEach(file => {
//     const model = sequelize['import'](path.join(__dirname, file));
//     db[model.name] = model;
//   });

// Object.keys(db).forEach(modelName => {
//   if (db[modelName].associate) {
//     db[modelName].associate(db);
//   }
// });

// db.sequelize = sequelize;
// db.Sequelize = Sequelize;

// module.exports = db;

